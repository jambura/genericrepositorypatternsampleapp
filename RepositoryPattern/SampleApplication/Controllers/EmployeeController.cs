﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleApplication.Interface;
using SampleApplication.Models;

namespace SampleApplication.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IGenericRepository<Employee> employeeRepository;

        public EmployeeController(IGenericRepository<Employee> _employeeRepository)
        {
            employeeRepository = _employeeRepository;
        }
        public IActionResult Index()
        {
            var employeeData = employeeRepository.GetAll();
            return View();
        }
    }
}