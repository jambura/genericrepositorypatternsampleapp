﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleApplication.Interface;
using SampleApplication.Models;

namespace SampleApplication.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IGenericRepository<Department> departmentRepository;

        public DepartmentController(IGenericRepository<Department> _departmentRepository)
        {
            departmentRepository = _departmentRepository;
        }
        public IActionResult Index()
        {
            var departmentData = departmentRepository.GetAll();
            return View();
        }
    }
}